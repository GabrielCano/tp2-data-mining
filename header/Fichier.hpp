#ifndef FICHIER_H
#define FICHIER_H

#include <map>
#include <string>
#include <fstream>
#include <iostream>
#include <locale>

extern std::map<std::string,std::string> nsf_prog;

class Corpus;
class Kmeans;

class Fichier {
  friend class Corpus;
  friend class Kmeans;
  friend class Cluster;
  unsigned int nombre_total_mot;
  std::string chemin_fichier;
  std::string nsf;
  std::map<std::string,double> nombre_occurence;
  bool contient(std::string key)const;
  Fichier();
public:
  Fichier(std::string _chemin_fichier);
  std::string to_string()const;
  const std::string& getChemin(void)const;
};

bool operator==(const Fichier& f1, const Fichier& f2);
bool operator!=(const Fichier& f1, const Fichier& f2);
std::ostream& operator<<(std::ostream &strm,const Fichier &fichier);
#endif
