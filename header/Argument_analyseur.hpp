#ifndef ARG_H
#define ARG_H

#include "constants.hpp"

#include <chrono>
#include <ctime>
#include <string>
#include <queue>
#include <iostream>
#include <cstdlib>

//Definition des noms des options :
#define OPT_DONNEE_EMPLACEMENT "--src"
#define OPT_DONNEE_DESTINATION "--dest"
#define OPT_NOMBRE_THREAD "--thread"
#define OPT_CHRONO "--chrono"
#define OPT_PRETRAITEMENT "--analyse-donnee"
#define OPT_CLUSTERISER "--clusteriser"
#define OPT_ITERATION "--iteration"

//Definition de macro pour les messages :

extern std::queue<std::string> rep;
extern std::queue<std::string> fich;

class Argument_analyseur{
  std::chrono::time_point<std::chrono::system_clock> start, end;

  void faire_emplacement_donnee(int* pos,int taille, char** arguments);
  void faire_destination_pretraitement(int* pos,int taille, char** arguments);
  void faire_thread_pretraitement(int* pos,int taille, char** arguments);
  void faire_iteration(int* pos,int taille, char** arguments);
  void faire_chronometre(void);
  void faire_traitement(void);
  void faire_cluster(void);
  bool est_option_valide(std::string option) const;
public:
  bool active_chrono = false;
  bool est_emplacement_donnee = false;
  bool est_destination_pretraitement = false;
  bool est_nombre_thread = false;
  bool est_nombre_iteration = false;
  bool faire_pretraitement = false;
  bool faire_clusterisation = false;
  Argument_analyseur(int nombre_argument, char** arguments);
  ~Argument_analyseur();
  std::string emplacement_donnee;
  std::string destination_pretraitement;
  int nombre_thread_pretraitement;
  int nombre_iteration;
};

#endif
