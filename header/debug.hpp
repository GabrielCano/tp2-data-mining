#ifndef DEBUG_H
#define DEBUG_H

#include <map>
#include <iostream>

namespace debug {
  void print_map(const std::map<std::string,double>& _map, const std::string& msg);
}

#endif
