#include "Kmeans.hpp"




Kmeans::Kmeans(Corpus const& corpus, int _nombre_cluster, int _nombre_thread)
:nombre_thread(_nombre_thread), nombre_cluster(_nombre_cluster){
  srand(time(NULL));
  corpus.getListeTerme(liste_termes);
  const std::vector<Fichier>& liste_fichiers = corpus.getFichiers();
  for(auto it = liste_fichiers.begin() ; it != liste_fichiers.end() ; it++){
    liste_donnees.push_back(&(*it));
  }

  liste_clusters = new Cluster[nombre_cluster];
}

Kmeans::~Kmeans(){
  delete[] liste_clusters;
}

void Kmeans::init_clustering_iteration(){
  //initialisation
  std::vector<unsigned int> init_point_ind;
  int premier_ind = rand()%liste_donnees.size();
  //1 choisir le premier point au hasard.
  init_point_ind.push_back(premier_ind);
  /*
  */
  int state = 0, max_state = nombre_cluster + liste_donnees.size();
  for(unsigned int i = 1; i < nombre_cluster; i++){
    bool est_deja_pris = true;
    unsigned int ind = rand()%liste_donnees.size();
    while(est_deja_pris){
      est_deja_pris = false;
      for(auto it = init_point_ind.begin() ; it != init_point_ind.end() ; it++){
        if(*it == ind){
          ind = rand()%liste_donnees.size();
          est_deja_pris = true;
          break;
        }
      }
    }
    init_point_ind.push_back(ind);
    liste_clusters[i].clear();
    liste_clusters[i].add(liste_donnees[ind]);
    //liste_clusters[i].calcule_centre();
    std::cerr << "\rInitialisation des centres : " << convert_pourcentage(++state,max_state);
  }
  //init_point.push_back(liste_donnees[premier_ind]);
  //for(unsigned int i = 1; i < nombre_cluster; i++){
  //  //2 choisir le k+1 le plus eloigné de tous les autres.
  //  //calcule de la distance entre les points choisi et le candidat
  //  double max = -1;
  //  int ind = -1;
  //  for(unsigned int k = 0 ; k < liste_donnees.size() ; k++){
  //    bool stop = false;
  //    double dist = 0;
  //    for(unsigned int j = 0 ; j < init_point_ind.size() ; j++){
  //      double temp = distance_euclidienne(liste_donnees[init_point_ind[j]],liste_donnees[k] );
  //      stop = (temp == 0);
  //      if(stop){break;}
  //      dist += temp;
  //    }
  //    if(stop){continue;}
  //    if( max == -1 || dist < max ){
  //      max = dist;
  //      ind = k;
  //    }
  //  }
  //  //init_point.push_back(liste_donnees[ind]);
  //  init_point_ind.push_back(ind);
  //  liste_clusters[i].add(liste_donnees[ind]);
  //  //liste_clusters[i].calcule_centre();
  //}
  std::cerr << std::endl;
  //Initialisation de la queue des points à traité
  for(unsigned int i = 0 ; i < liste_donnees.size() ; i++){
    std::cerr << "\rInitialisation des centres : " << convert_pourcentage(++state,max_state);
    bool est_graine = false;
    for(auto j = init_point_ind.begin() ; j != init_point_ind.end() ; j++){
      if( i == *j ){
        est_graine = true;
        break;
      }
    }
    if(est_graine){continue;}
    queue_point_restant.push(liste_donnees[i]);
  }
}

void Kmeans::init_next_iteration(){
  //On considère que les clusters on deja des centres
  //On ajoute les points les plus proche de ces centres
  unsigned int state = 0;
  unsigned int max_state = nombre_cluster*liste_donnees.size() + liste_donnees.size();
  std::vector<unsigned int> init_point_ind;
  for(unsigned int i = 0; i < nombre_cluster; i++){
    double min = -1;
    int ind = -1;
    for(unsigned int k = 0 ; k < liste_donnees.size() ; k++){
      std::cerr << "\r\tInitialisation des centres : " << convert_pourcentage(++state,max_state) << "%" << std::flush;
      double dist = distance_euclidienne(liste_donnees[k], &liste_clusters[i],&liste_termes);
      if( min == -1 || dist < min ){
        min = dist;
        ind = k;
      }
    }
    init_point_ind.push_back(ind);
    liste_clusters[i].clear();
    liste_clusters[i].add(liste_donnees[ind]);
    //liste_clusters[i].calcule_centre();
  }
  //Initialisation de la queue des points à traité
  for(unsigned int i = 0 ; i < liste_donnees.size() ; i++){
    bool est_graine = false;
    std::cerr << "\r\tInitialisation des centres : " << convert_pourcentage(++state,max_state) << "%" << std::flush;
    for(auto j = init_point_ind.begin() ; j < init_point_ind.end() ; j++){
      if( i == *j ){
        est_graine = true;
        break;
      }
    }
    if(est_graine){continue;}
    queue_point_restant.push(liste_donnees[i]);
  }
  std::cerr << std::endl;
}


double Kmeans::distance_euclidienne(const Fichier* point, const Cluster* cluster, std::vector<std::string>* liste_termes){
  double result = 0;
  for(auto t = liste_termes->begin() ; t != liste_termes->end() ; t++ ){
    double x1 = 0 , x2 = 0;
    if(point->nombre_occurence.find(*t) != point->nombre_occurence.end()){
      x1 = point->nombre_occurence.at(*t);
    }
    if(cluster->centre.find(*t) != cluster->centre.end()){
      x2 = cluster->centre.at(*t);
    }
    double x = x1 - x2;
    //result += x*x;
    result += std::abs(x);
  }
  return result;
  //return std::sqrt(result);
}

double Kmeans::distance_euclidienne(const Cluster* cluster, std::vector<std::string>& liste_termes){
  double result = 0;
  for(auto t = liste_termes.begin() ; t != liste_termes.end() ; t++ ){
    double x1 = 0 , x2 = 0;
    if(cluster->vieux_centre.find(*t) != cluster->vieux_centre.end()){
      x1 = cluster->vieux_centre.at(*t);
    }
    if(cluster->centre.find(*t) != cluster->centre.end()){
      x2 = cluster->centre.at(*t);
    }
    double x = x1 - x2;
    //result += x*x;
    result += std::abs(x);
  }
  return result;
  //return std::sqrt(result);
}

double Kmeans::distance_euclidienne(const Fichier* point1, const Fichier* point2){
  double result = 0;
  for(auto t = liste_termes.begin() ; t != liste_termes.end() ; t++ ){
    std::string s(*t);
    double x1 = 0, x2 = 0;
    if(point1->nombre_occurence.find(*t) != point1->nombre_occurence.end()){
      x1 = point1->nombre_occurence.at(*t);
    }
    if(point2->nombre_occurence.find(*t) != point2->nombre_occurence.end()){
      x2 = point2->nombre_occurence.at(*t);
    }
    double x = x1 - x2;
    //result += x*x;
    result += std::abs(x);
  }
  return result;
  //return std::sqrt(result);
}

double Kmeans::distance_euclidienne(const Cluster* cluster1, const Cluster* cluster2, std::vector<std::string>& liste_termes){
  double result = 0;
  for(auto t = liste_termes.begin() ; t != liste_termes.end() ; t++ ){
    std::string s(*t);
    double x1 = 0, x2 = 0;
    if(cluster1->centre.find(*t) != cluster1->centre.end()){
      x1 = cluster1->centre.at(*t);
    }
    if(cluster2->centre.find(*t) != cluster2->centre.end()){
      x2 = cluster2->centre.at(*t);
    }
    double x = x1 - x2;
    //result += x*x;
    result += std::abs(x);
  }
  return result;
  //return std::sqrt(result);
}

void Kmeans::attribuer_cluster(std::queue<const Fichier*>* data, Cluster *clusters, unsigned int nombre_cluster, std::vector<std::string>* liste_termes, std::mutex* mtx, unsigned int state){
  if(clusters == NULL || mtx == NULL || data == NULL) {
    return;
  }
  bool fin = false;
  while(!fin){
    mtx->lock();
    if(data->empty()){
      fin = true;
      mtx->unlock();
      continue;
    }
    const Fichier* point = data->front();
    data->pop();
    std::cerr << "\r\tAttribution des clusters : " << (int)((((double)state-data->size())/state)*100) << "%" << std::flush;
    mtx->unlock();
    double min_dist = -1;
    unsigned min_ind = -1;
    for(unsigned int i = 0 ; i < nombre_cluster ; i++){
      const Cluster * cl = &clusters[i];
      double dist = distance_euclidienne(point, cl,liste_termes);
      if( (min_dist == -1) || (dist < min_dist) ){
        min_dist = dist;
        min_ind = i;
      }
    }
    clusters[min_ind].add(point);
  }
}

void Kmeans::clusteriser(unsigned int nombre_iteration_max, double _seuil_stabilite){
  std::thread threads[MAX_THREAD];
  bool est_stable = false;
  std::cerr << "Iteration max :" << nombre_iteration_max << std::endl;
  for(unsigned int i = 0 ; i < nombre_iteration_max && !est_stable ; i++ ){
    //Initialisation
    std::cerr << "Iteration : " << i << std::endl;
    est_stable = true;
    if(i == 0){
      init_clustering_iteration();
    }
    else{
      init_next_iteration();
    }
    //Generation des threads

    for(unsigned int j = 0 ; j < nombre_thread ; j++){
      threads[j] = std::thread(attribuer_cluster,&queue_point_restant,liste_clusters,nombre_cluster,&liste_termes,&mtx,queue_point_restant.size());
    }
    //Attente de la fin du calcul
    for(unsigned int j = 0 ; j < nombre_thread ; j++){
      if(threads[j].joinable()){
        threads[j].join();
      }
    }

    std::cerr << std::endl;
    std::cerr << "\tdebug : ";
    for(unsigned int j = 0 ; j < nombre_cluster ; j++){
      std::cerr << " " << liste_clusters[j].membres.size();
    }
    std::cerr << std::endl;

    //Mise a jour des centres des clusters
    for(unsigned int j = 0 ; j < nombre_cluster ; j++){
      liste_clusters[j].calcule_centre();
      std::cerr << "\r\tRecalcule des centres : " << convert_pourcentage(j,nombre_cluster) << "%" << std::flush;
    }
    std::cerr << "\r\tRecalcule des centres : " << convert_pourcentage(1,1) << "%" << std::flush;
    std::cerr << std::endl;
    //Mise a jour du critère de stabilité
    for(unsigned int j = 0 ; j < nombre_cluster ; j++){
      std::cerr << "\r\tMise a jour du critère de stabilité : "<< convert_pourcentage(j,nombre_cluster) << "%" << std::flush;
      if(distance_euclidienne(&liste_clusters[j],liste_termes) > _seuil_stabilite){
        est_stable = false;
        std::cerr << "\r\tMise a jour du critère de stabilité : "<< convert_pourcentage(1,1) << "%" << std::flush;
        break;
      }
    }
    std::cerr << std::endl;
  }
  std::cout << std::endl;
}

bool Kmeans::verifier_stabilite_iteration(Cluster* actuel, Cluster* nouveau,  double _seuil_stabilite){
  for(unsigned int i = 0 ; i < nombre_cluster ; i++){
    if( distance_euclidienne(actuel,nouveau,liste_termes) > _seuil_stabilite ){
      return false;
    }
  }
  return true;
}



void Kmeans::print_result(std::ostream& output){
  if(NULL != liste_clusters){
/*
    std::cerr << "\tdebug : ";
    for(unsigned int j = 0 ; j < nombre_cluster ; j++){
      std::cerr << " " << liste_clusters[j].membres.size();
    }
*/
    for(unsigned int i = 0 ; i < nombre_cluster ; i++){
      output << "Cluster " << i << " : ";
      std::map<std::string, int> temp;
      for (const Fichier* j : liste_clusters[i].membres){
        if(temp.find(j->nsf) != temp.end()){
          temp[j->nsf] += 1;
        }else{
          temp[j->nsf] = 1;
        }
      }
      output << "qualité = " << temp.size() << std::endl;
      for(auto it = temp.begin(); it != temp.end() ; it++){
        output << "\t" << it->first << " : "<< it->second << std::endl;
      }
      output << "----------------------------------------" << std::endl;
    }
    return;
  }
  output << "Aucun cluster n'a été calculé" << std::endl;
}

int convert_pourcentage(unsigned int val,unsigned int max){
  return (int)((((double)val)/max)*100);
}
