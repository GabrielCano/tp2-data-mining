#include <cstdlib>
#include <iostream>
#include <thread>
#include <mutex>
#include <csignal>

#include "Outils.hpp"
#include "Fichier.hpp"
#include "Corpus.hpp"
#include "Kmeans.hpp"
#include "Argument_analyseur.hpp"


void signal_handler(int signal);

std::mutex mtx;
std::mutex mtx_elagage;
std::queue<std::string> rep;
std::queue<std::string> fich;
std::map<std::string,std::string> nsf_prog;
unsigned int count = 0;
Argument_analyseur *analyseur = NULL;
std::thread liste_thread[MAX_THREAD];
/*
arg1 : emplacement des donnée brute des article (versions courte)
arg2 : emplacement où seront copié les données une fois les pré-traitement fini.
arg3 : nombre de thread
*/
int main(int argc, char** argv){
  analyseur = new Argument_analyseur(argc,argv);
  std::signal(SIGINT, signal_handler);
  // Initialisation des variables :
  /*
  */
  if( analyseur->faire_pretraitement ){
    std::cout << "\nNombre de thread de prétraitement : " << analyseur->nombre_thread_pretraitement << std::endl;
    outils::get_liste_repertoire(analyseur->emplacement_donnee,rep);
    outils::developper_list_fichier_si_besoin(rep,fich);
    for(int i = 0 ; i < analyseur->nombre_thread_pretraitement ; i++){
      liste_thread[i] = std::thread(outils::explore_fichiers,analyseur->destination_pretraitement);
    }

    for(int i = 0 ; i < analyseur->nombre_thread_pretraitement ; i++){
      if(liste_thread[i].joinable()){
        liste_thread[i].join();
      }
    }
  }

  std::vector<Fichier> fichiers;
  if( analyseur->faire_clusterisation ){
    outils::analyse_repertoire(analyseur->destination_pretraitement,fichiers);
    Corpus c(fichiers,analyseur->nombre_thread_pretraitement);
    Kmeans kmean(c,30,analyseur->nombre_thread_pretraitement);
    kmean.clusteriser(analyseur->nombre_iteration);
    std::ofstream output("result.txt");
    if(output){
      kmean.print_result(output);
      output.close();
    }
    else {
      kmean.print_result(std::cout);
    }
  }

  delete analyseur;
  return EXIT_SUCCESS;
}


void signal_handler(int signal){
  std::cout << std::endl;
  if(analyseur != NULL){
    delete analyseur;
  }
  exit(EXIT_FAILURE);
}
