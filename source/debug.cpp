#include "debug.hpp"

void debug::print_map(const std::map<std::string,double>& _map, const std::string& msg){
  std::cerr << "debug : printing map : " << msg << std::endl;
  for (auto it = _map.begin() ; it != _map.end() ; it++){
    std::cerr << it->first << " : " << it->second << std::endl;
  }

}
