#include "Argument_analyseur.hpp"

Argument_analyseur::Argument_analyseur(int nombre_argument, char** arguments)
: nombre_thread_pretraitement(NOMBRE_THREAD_DEFAUT),nombre_iteration(NOMBRE_ITERATION_DEFAUT){
  for(int i = 1 ; i < nombre_argument ; i++){
    if(!est_option_valide(arguments[i])){
      std::cout  << (arguments[i]) << ARGUMENT_NON_VALIDE_MESSAGE << std::endl;
      continue;
    }
    std::string arg(arguments[i]);
    if(arg.compare(OPT_DONNEE_EMPLACEMENT) == 0){
      faire_emplacement_donnee(&i,nombre_argument,arguments);
    }
    else if (arg.compare(OPT_DONNEE_DESTINATION) == 0){
      faire_destination_pretraitement(&i,nombre_argument,arguments);
    }
    else if (arg.compare(OPT_NOMBRE_THREAD) == 0){
      faire_thread_pretraitement(&i,nombre_argument,arguments);
    }
    else if (arg.compare(OPT_CHRONO) == 0){
      faire_chronometre();
    }
    else if (arg.compare(OPT_PRETRAITEMENT) == 0){
      faire_traitement();
    }
    else if (arg.compare(OPT_CLUSTERISER) == 0){
      faire_cluster();
    }
    else if (arg.compare(OPT_ITERATION) == 0){
      faire_iteration(&i,nombre_argument,arguments);
    }
  }

  bool est_erreur = false;
  bool est_alerte = false;
  std::string erreur_message;
  std::string alerte_message;
  if(!est_emplacement_donnee){
    est_erreur = true;
    erreur_message += EMPLACEMENT_DONNEE_MANQUANT_MESSAGE;
  }
  if(!est_destination_pretraitement){
    est_erreur = true;
    erreur_message += DESTINATION_DONNEE_PRETRAITEMENT_MANQUANT_MESSAGE;
  }
  if(!est_nombre_thread){
    nombre_thread_pretraitement = NOMBRE_THREAD_DEFAUT;
    alerte_message += AUCUN_NOMBRE_THREAD_MESSAGE(nombre_thread_pretraitement);
    est_alerte = true;
  }
  if(!est_nombre_iteration){
    nombre_iteration = NOMBRE_ITERATION_DEFAUT;
    alerte_message += ITERATION_INFO_MESSAGE + std::to_string(nombre_iteration);
    est_alerte = true;
  }

  if(est_alerte){
    std::cout << alerte_message;
  }
  if(est_erreur){
    std::cout << erreur_message;
    exit(EXIT_FAILURE);
  }

  if(active_chrono){
    start = std::chrono::system_clock::now();
  }
}

Argument_analyseur::~Argument_analyseur(){
  if(active_chrono){
    end = std::chrono::system_clock::now();
    int elapsed_seconds = std::chrono::duration_cast<std::chrono::seconds>(end-start).count();
    std::time_t end_time = std::chrono::system_clock::to_time_t(end);
    int h = elapsed_seconds/3600;
    int mn = (elapsed_seconds%3600)/60;
    int sec = (elapsed_seconds%3600)%60;
    std::cerr << "Execution fini a : " << std::ctime(&end_time) << "temps  " << h << "h" << mn << "mn" << sec << "s\n";
  }
}

bool Argument_analyseur::est_option_valide(std::string option) const{
  return (
    (option.compare(OPT_DONNEE_EMPLACEMENT) == 0) ||
    (option.compare(OPT_DONNEE_DESTINATION) == 0) ||
    (option.compare(OPT_NOMBRE_THREAD) == 0) ||
    (option.compare(OPT_PRETRAITEMENT) == 0) ||
    (option.compare(OPT_CLUSTERISER) == 0) ||
    (option.compare(OPT_ITERATION) == 0) ||
    (option.compare(OPT_CHRONO) == 0)
  );
}


void Argument_analyseur::faire_emplacement_donnee(int* pos,int taille, char** arguments){
  if(*pos + 1 < taille){
    std::string arg(arguments[*pos + 1]);
    if(arg.length() > 2 && arg[0] == '-' && arg[1] == '-'){
      std::cout << MANQUE_INFO << arguments[*pos] << std::endl;
    }
    //On considère que l'argument est valide (sauf si 1 caractère -> a décider)
    emplacement_donnee = arg;
    est_emplacement_donnee = true;
    (*pos)++;
  }
  else{
    std::cout << MANQUE_INFO << arguments[*pos] << std::endl;
  }
}

void Argument_analyseur::faire_destination_pretraitement(int* pos,int taille, char** arguments){
  if(*pos + 1 < taille){
    std::string arg(arguments[*pos+1]);
    if(arg.length() > 2 && arg[0] == '-' && arg[1] == '-'){
      std::cout << MANQUE_INFO << arguments[*pos] << std::endl;
    }
    //On considère que l'argument est valide (sauf si 1 caractère -> a décider)
    destination_pretraitement = arg;
    est_destination_pretraitement = true;
    (*pos)++;
  }
  else{
    std::cout << MANQUE_INFO << arguments[*pos] << std::endl;
  }
}

void Argument_analyseur::faire_thread_pretraitement(int* pos,int taille, char** arguments){
  if(*pos + 1 < taille){
    std::string arg(arguments[*pos+1]);
    if(arg.length() > 2 && arg[0] == '-' && arg[1] == '-'){
      std::cout << MANQUE_INFO << arguments[*pos] << std::endl;
      return;
    }
    //On considère que l'argument est valide (sauf si 1 caractère -> a décider)
    int n = atoi(arg.c_str());
    if(n == 0){
      std::cout << NOMBRE_TRHEAD_NON_VALIDE_MESSAGE(arg) << std::endl;
    }else{
      nombre_thread_pretraitement = (n >= MAX_THREAD)?MAX_THREAD:n;
      est_nombre_thread = true;
    }
    (*pos)++;
  }
  else{
    std::cout << MANQUE_INFO << arguments[*pos] << std::endl;
  }
}

void Argument_analyseur::faire_chronometre(void){
  active_chrono = true;
}

void Argument_analyseur::faire_traitement(void){
  faire_pretraitement = true;
}

void Argument_analyseur::faire_cluster(void){
  faire_clusterisation = true;
}

void Argument_analyseur::faire_iteration(int* pos,int taille, char** arguments){
  if(*pos + 1 < taille){
    std::string arg(arguments[*pos+1]);
    if(arg.length() > 2 && arg[0] == '-' && arg[1] == '-'){
      std::cout << MANQUE_INFO << arguments[*pos] << std::endl;
      return;
    }
    //On considère que l'argument est valide (sauf si 1 caractère -> a décider)
    int n = atoi(arg.c_str());
    if(n == 0){
      std::cout << ITERATION_INSUFFISANTE_MESSAGE << std::endl;
    }else{
      nombre_iteration = n;
      est_nombre_iteration = true;
    }
    (*pos)++;
  }
  else{
    std::cout << MANQUE_INFO << arguments[*pos] << std::endl;
  }
}
