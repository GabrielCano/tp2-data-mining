#include "Cluster.hpp"

Cluster::Cluster(const Fichier* init_point){
  membres.push_back(init_point);
}

Cluster::Cluster(){}

void Cluster::add(const Fichier* nouveau_membre){
  mtx.lock();
  membres.push_back(nouveau_membre);
  mtx.unlock();
}

void Cluster::clear(){
  membres.clear();
}

void Cluster::calcule_centre(){
  vieux_centre = centre;
  for(auto m = membres.begin() ; m != membres.end() ; m++){
    for(auto it = (*m)->nombre_occurence.begin() ; it != (*m)->nombre_occurence.end() ; it++){
        centre[it->first] += it->second/membres.size();
    }
  }
}
